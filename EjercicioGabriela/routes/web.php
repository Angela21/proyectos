<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Viviendas','Casas@index', function() {
    return view('viviendas');
});
/*Route::post('/nueva','Casas@InsertarV');*/
/*Route::post('/nueva','Casas@InsertarV');*/
Route::post('/InsertarV','Casas@InsertarV');
Route::get('/nueva','Casas@casa');
Route::get('/casa','Casas@index')->name('casa');