<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model
{
    //
    public $timestamps = false;
    protected $table = 'viviendas';
    protected $fillable = ['c_habit','c_baños','c_colonia','precio','tamanio','municipio','departamento','categoria','negociable','estado'];
}
