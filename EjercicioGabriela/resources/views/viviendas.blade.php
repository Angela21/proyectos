<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viviendas</title>
	<link rel="stylesheet" href="{{('props/bootstrap/css/bootstrap.min.css')}}">
</head>
<body><br><br>
	<center>
	<div class="container">
	<br><br>
	<div>
		<a href="nueva" class="btn btn-primary">Nueva Vivienda</a>
		<!-- <input type="button" class="btn btn-primary" value="Guardar"> -->
	</div>
	<br><br>
	<table border="2" class="table table-dark table-hover">
		<thead>
			<tr>
				<td>ID</td>
				<td>Habitaciones</td>
				<td>Baños</td>
				<td>Colonia</td>
				<td>Precio</td>
				<td>Tamaño</td>
				<td>Municipio</td>
				<td>Departamento</td>
				<td>Categoria</td>
				<td>Negociable</td>
				<td>Estado</td>
			</tr>
		</thead>
	<tbody>
		<?php $n=1; foreach($vivienda as $vi) { ?>
			<tr>
				<td><?php echo $n++; ?></td>
				<td><?php echo $vi->c_habit ?></td>
				<td><?php echo $vi->c_baños ?></td>
				<td><?php echo $vi->c_colonia ?></td>
				<td><?php echo $vi->precio ?></td>
				<td><?php echo $vi->tamanio ?></td>
				<td><?php echo $vi->municipio ?></td>
				<td><?php echo $vi->departamento ?></td>
				<td><?php echo $vi->categoria ?></td>
				<td><?php echo $vi->negociable ?></td>
				<td><?php echo $vi->estado ?></td>
				
			</tr>
		<?php } ?>
	</tbody>
</table>
</body>
</div>
</center>
</html>