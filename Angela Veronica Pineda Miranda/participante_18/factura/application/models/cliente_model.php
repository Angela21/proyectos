<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class cliente_model extends CI_Model
{
	
	public function get_cliente(){
		$this->db->select('c.id_cliente, c.nombre, c.apellido, c.direccion, c.fecha_nacimiento, c.telefono, c.email');
		$this->db->from('cliente c');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function eliminar($id){
		$this->db->where('id_cliente',$id);
		return($this->db->delete('cliente'));
	}

	public function eliminarf($id){
		$this->db->where('id_cliente',$id);
		return($this->db->delete('cliente'));
	}

	public function set_cliente($datos){
		$this->db->set('nombre', $datos['nombre']);
		$this->db->set('apellido',$datos['apellido']);
		$this->db->set('direccion',$datos['direccion']);
		$this->db->set('fecha_nacimiento',$datos['fecha_nacimiento']);
		$this->db->set('telefono', $datos['telefono']);
		$this->db->set('email',$datos['email']);
		$this->db->insert('cliente');
	}

	public function get_factura($id){
		$this->db->select('f.num_factura, c.nombre, f.fecha, p.nombre');
		$this->db->from('factura f');
		$this->db->join('cliente c','c.id_cliente = f.id_cliente');
		$this->db->join('modo_pago p','p.num_pago = f.num_pago');

		$this->db->where('c.id_cliente',$id);
		$exe = $this->db->get();
		if($exe->num_rows()>0){
			return $exe->result();
		}else{
			return false;
		}
	}

	public function get_datos($id){
		$this->db->where('id_cliente',$id);
		$exe = $this->db->get('cliente');
		return $exe->result();
	}

	public function actualizar($datos){
		$this->db->set('nombre', $datos['nombre']);
		$this->db->set('apellido',$datos['apellido']);
		$this->db->set('direccion',$datos['direccion']);
		$this->db->set('fecha_nacimiento',$datos['fecha_nacimiento']);
		$this->db->set('telefono', $datos['telefono']);
		$this->db->set('email',$datos['email']);
		$this->db->where('id_cliente',$datos['id_cliente']);
		$this->db->update('cliente');
	}
}
 ?>