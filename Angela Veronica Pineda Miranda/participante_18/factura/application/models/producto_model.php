<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_model extends CI_Model
{
	
	public function get_producto(){
		$this->db->select('p.id_producto, p.nombre, p.precio, p.stock, c.nombre_c');
		$this->db->from('producto p');
		$this->db->join('categoria c','c.id_categoria = p.id_categoria');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function eliminar($id){
		$this->db->where('id_producto',$id);
		return($this->db->delete('producto'));
	}

	public function get_categoria(){
		$exe = $this->db->get('categoria');
		return $exe->result();
	}

	public function set_producto($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('precio',$datos['precio']);
		$this->db->set('stock',$datos['stock']);
		$this->db->set('id_categoria',$datos['categoria']);
		$this->db->insert('producto');
	}

	public function get_datos($id){
		$this->db->where('id_producto',$id);
		$exe = $this->db->get('producto');
		return $exe->result();
	}

	public function actualizar($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('precio',$datos['precio']);
		$this->db->set('stock',$datos['stock']);
		$this->db->set('id_categoria',$datos['categoria']);
		$this->db->where('id_producto',$datos['id_producto']);
		$this->db->update('producto');
	}
}
 ?>