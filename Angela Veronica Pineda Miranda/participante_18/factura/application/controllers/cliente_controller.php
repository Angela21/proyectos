<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class cliente_controller extends CI_Controller
{
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('cliente_model');
	}

	public function index(){
		$data = array('title' => 'Prueba Diagnostica || Cliente',
			'cliente' => $this->cliente_model->get_cliente());
		$this->load->view('template/header',$data);
		$this->load->view('cliente_view');
		$this->load->view('template/footer');
	}

	public function eliminar($id){
		$this->cliente_model->eliminar($id);
		redirect('cliente_controller/index','refresh');	
	}

	public function eliminarf($id){
		$this->cliente_model->eliminarf($id);
		redirect('cliente_controller/index','refresh');	
	}

	public function ingresar(){
		$datos['nombre'] = $_POST['nombre'];
		$datos['apellido'] = $_POST['apellido'];
		$datos['direccion'] = $_POST['direccion'];
		$datos['fecha_nacimiento'] = $_POST['fecha_nacimiento'];
		$datos['telefono'] = $_POST['telefono'];
		$datos['email'] = $_POST['email'];
		$this->cliente_model->set_cliente($datos);
		
		redirect('cliente_controller/index','refresh');
	}


	public function get_factura($id){
		$datos = array('title' => 'Factura del cliente',
			'factura' => $this->cliente_model->get_factura($id));
		$this->load->view('template/header',$datos);
		$this->load->view('factura_view');
		$this->load->view('template/footer');
	}

	public function get_datos($id){
		$datos = array('title' => 'Actualizar cliente',
			'cliente' => $this->cliente_model->get_datos($id));
		$this->load->view('template/header',$datos);
		$this->load->view('cliente_viewA');
		$this->load->view('template/footer');
	}

	public function actualizar(){
		$datos['id_cliente'] = $_POST['id_cliente'];
		$datos['nombre'] = $_POST['nombre'];
		$datos['apellido'] = $_POST['apellido'];
		$datos['direccion'] = $_POST['direccion'];
		$datos['fecha_nacimiento'] = $_POST['fecha_nacimiento'];
		$datos['telefono'] = $_POST['telefono'];
		$datos['email'] = $_POST['email'];
		$this->cliente_model->actualizar($datos);
		
		redirect('cliente_controller/index','refresh');
	}
}
 ?>