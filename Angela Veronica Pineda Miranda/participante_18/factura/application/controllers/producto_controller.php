<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class producto_controller extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent:: __construct();
 		$this->load->model('producto_model');
 	}

 	public function index(){
 		$data = array('title' => 'Prueba Diagnostica || Producto',
 			'producto' => $this->producto_model->get_producto(),
 			'categoria' => $this->producto_model->get_categoria());
 		$this->load->view('template/header',$data);
 		$this->load->view('producto_view');
 		$this->load->view('template/footer');
 	}

 	public function eliminar($id){
 		$this->producto_model->eliminar($id);
 		redirect('producto_controller','refresh');
 	}

 	public function ingresar(){
 		$datos['nombre'] = $_POST['nombre'];
 		$datos['precio'] = $_POST['precio'];
 		$datos['stock'] = $_POST['stock'];
 		$datos['categoria'] = $_POST['categoria'];
 		$this->producto_model->set_producto($datos);
 		redirect('producto_controller/index','refresh');
 	}

 	public function get_datos($id){
		$datos = array('title' => 'Actualizar producto',
			'producto' => $this->producto_model->get_datos($id),
			'categoria' => $this->producto_model->get_categoria());
		$this->load->view('template/header',$datos);
		$this->load->view('producto_viewA');
		$this->load->view('template/footer');
	}

	public function actualizar(){
		$datos['id_producto'] = $_POST['id_producto'];
		$datos['nombre'] = $_POST['nombre'];
 		$datos['precio'] = $_POST['precio'];
 		$datos['stock'] = $_POST['stock'];
 		$datos['categoria'] = $_POST['categoria'];
		$this->producto_model->actualizar($datos);
		
		redirect('producto_controller/index','refresh');
	}
 } 
 ?>