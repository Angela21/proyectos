<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class factura extends CI_Controller
{
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('factura_model');
	}

	public function index(){
		$data = array('title' => 'Prueba Diagnostica || factura',
			'factura' => $this->factura_model->get_factura());
		$this->load->view('template/header',$data);
		$this->load->view('factura_view');
		$this->load->view('template/footer');
	}
}
 ?>