<body>
	<div class="container">
		<?php foreach ($producto as $p) { ?>
		<form action="<?= base_url('producto_controller/actualizar') ?>" method="POST">
			<table>
				<tr>
					<input type="hidden" name="id_producto" value="<?= $p->id_producto ?>">
					<td><label>Nombre</label></td>
					<td><input type="text" name="nombre" value="<?= $p->nombre ?>"></td>
				</tr>
				<tr>
					<td><label>Precio</label></td>
					<td><input type="decimal" name="precio" value="<?= $p->precio ?>"></td>
				</tr>
				<tr>
					<td><label>stock</label></td>
					<td><input type="number" name="stock" value="<?= $p->stock ?>"></td>
				</tr>
				<tr>
					<td>Categoria</td>
					<td>
						<select name="categoria">
							<option value="">--Seleccione una opcion--</option>
							<?php foreach ($categoria as $c ) { ?>
								<option value="<?= $c->id_categoria ?>" <?php if($c->id_categoria == $p->id_categoria){ echo 'selected'; } ?> ><?= $c->nombre_c ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><input type="submit" value="Guardar"></td>
				</tr>
			</table>
		</form>
	<?php } ?>
	</div>