<body>
	<div class="container">
		<?php foreach ($cliente as $c) { ?>
		<form action="<?= base_url('cliente_controller/actualizar') ?>" method="POST">
			<table>
				<tr>
					<input type="hidden" name="id_cliente" required="" value="<?= $c->id_cliente ?>">
					<td><label>Nombre</label></td>
					<td><input type="text" name="nombre" value="<?= $c->nombre ?>"></td>
				</tr>
				<tr>
					<td><label>Apellido</label></td>
					<td><input type="text" name="apellido" value="<?= $c->apellido ?>"></td>
				</tr>
				<tr>
					<td><label>Direccion</label></td>
					<td><input type="text" name="direccion" value="<?= $c->direccion ?>"></td>
				</tr>
				<tr>
					<td><label>Fecha de nacimiento</label></td>
					<td><input type="date" name="fecha_nacimiento" value="<?= $c->fecha_nacimiento ?>"></td>
				</tr>
				<tr>
					<td><label>Telefono</label></td>
					<td><input type="text" name="telefono" value="<?= $c->telefono ?>"></td>
				</tr>
				<tr>
					<td><label>Email</label></td>
					<td><input type="text" name="email" value="<?= $c->email ?>"></td>
				</tr>
				<tr>
					<td><input type="submit"  value="Guardar"></td>
				</tr>
			</table>
		</form>
	<?php } ?>
	</div>