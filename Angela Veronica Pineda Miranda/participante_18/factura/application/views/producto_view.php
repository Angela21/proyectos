<body>
	<center><h3>Productos</h3></center>
	<div class="container">
		<table border="1" class="table table-dark">
			<thead>
				<tr>
					<th>ID</th>
					<th>nombre</th>
					<th>precio</th>
					<th>stock</th>
					<th>categoria</th>
					<th>Eliminar</th>
					<th>Actualizar</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($producto as $p) { ?>
					<tr>
						<td><?= $p->id_producto ?></td>
						<td><?= $p->nombre ?></td>
						<td><?= $p->precio ?></td>
						<td><?= $p->stock ?></td>
						<td><?= $p->nombre_c ?></td>
						<td><a href="<?= base_url('producto_controller/eliminar/'.$p->id_producto) ?>"><button class="btn btn-danger">Eliminar</button></a></td>
						<td><a href="<?= base_url('producto_controller/get_datos/'.$p->id_producto) ?>"><button class="btn btn-success">Actualizar</button></a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<br><br><br>

		<div class="container">
		<form action="<?= base_url('producto_controller/ingresar') ?>" method="POST">
			<table>
				<tr>
					<td><label style="font-family: 'Monserrat'">Nombre</label></td>
					<td><input type="text" name="nombre" required=""></td>
				</tr>
				<tr>
					<td><label style="font-family: 'Monserrat'">Precio</label></td>
					<td><input type="decimal" name="precio" required=""></td>
				</tr>
				<tr>
					<td><label style="font-family: 'Monserrat'">stock</label></td>
					<td><input type="number" name="stock" required=""></td>
				</tr>
				<tr>
					<td><label style="font-family: 'Monserrat'">Categoria</label></td>
					<td>
						<select name="categoria" required="">
							<option value="">--Seleccione una opcion--</option>
							<?php foreach ($categoria as $c ) { ?>
								<option value="<?= $c->id_categoria ?>"><?= $c->nombre_c ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><input type="submit" value="Guardar"></td>
				</tr>
			</table>
		</form>
	</div>