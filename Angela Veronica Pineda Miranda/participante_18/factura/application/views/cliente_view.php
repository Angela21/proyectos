<body>
<center><h3>Clientes</h3></center>
<div class="container">
	<table border="1" class="table table-dark">
		<thead>
			<tr>
				<td>ID</td>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>Direccion</td>
				<td>Fecha de nacimiento</td>
				<td>Telefono</td>
				<td>Email</td>
				<td>Factura</td>
				<td>Eliminar</td>
				<td>Actualizar</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($cliente as $c) { ?>  
				<tr>
					<td><?= $c->id_cliente ?></td>
					<td><?= $c->nombre ?></td>
					<td><?= $c->apellido ?></td>
					<td><?= $c->direccion ?></td>
					<td><?= $c->fecha_nacimiento ?></td>
					<td><?= $c->telefono ?></td>
					<td><?= $c->email ?></td>
					<td><a href="<?= base_url('cliente_controller/get_factura/'.$c->id_cliente) ?>"><button class="btn btn-Warning">Ver factura</button></a></td>
					<td><a href="<?= base_url('cliente_controller/eliminar/'.$c->id_cliente) ?>"><button class="btn btn-danger">Eliminar</button></a></td>
					<td><a href="<?= base_url('cliente_controller/get_datos/'.$c->id_cliente) ?>"><button class="btn btn-success">Actualizar</button></a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<br><br><br>

	<div class="container">
		<form action="<?= base_url('cliente_controller/ingresar') ?>" method="POST">
			<table>
				<tr>
					<td><label>Nombre</label></td>
					<td><input type="text" name="nombre" required=""></td>
				</tr>
				<tr>
					<td><label>Apellido</label></td>
					<td><input type="text" name="apellido" required=""></td>
				</tr>
				<tr>
					<td><label>Direccion</label></td>
					<td><input type="text" name="direccion" required=""></td>
				</tr>
				<tr>
					<td><label>Fecha de nacimiento</label></td>
					<td><input type="date" name="fecha_nacimiento" required=""></td>
				</tr>
				<tr>
					<td><label>Telefono</label></td>
					<td><input type="text" name="telefono" required=""></td>
				</tr>
				<tr>
					<td><label>Email</label></td>
					<td><input type="text" name="email" required=""></td>
				</tr>
				<tr>
					<td><input type="submit"  value="Guardar" required=""></td>
				</tr>
			</table>
		</form>
	</div>