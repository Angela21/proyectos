<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>

	<link rel="stylesheet" type="text/css" href="<?= base_url('props/bootstrap/css/bootstrap.min.css') ?>">

	<script src="<?= base_url('props/bootstrap/js/bootstrap.min.js') ?>"></script>
</head>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#" style="font-family: 'Arial',cursive;color: green">Prueba Diagnostica</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		<div class="navbar-nav">
			<a class="nav-item nav-link active" href="<?= base_url('/producto_controller') ?>" style="font-family: 'Monserrat'">Producto <span class="sr-only">(current)</span></a>
		</div>
		<div class="navbar-nav">
			<a class="nav-item nav-link active" href="<?= base_url('/cliente_controller') ?>" style="font-family: 'Monserrat'">Cliente <span class="sr-only">(current)</span></a>
		</div>
	</div>
</nav>