<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\producto as UserModel;

class Home extends BaseController

{

	public function index()
	{
		$obj = new UserModel;
		$datos = $obj->get_producto();
		$sexo = $obj->get_sexo();
		return view('home',['mostrar'=>$datos,'sexo'=>$sexo]);
	}


	public function eliminar($id){
		$obj = new UserModel;
		$obj->eliminar($id);
	}

	public function ingresar(){
		$obj = new UserModel;
		$datos([
			'nombre' => $_POST['nombre'],
			'apellido' => $_POST['apellido'],
			'nombre_artistico'=> $_POST['nombre_artistico'],
			'id_sexo' =>$_POST['sexo']]);
		$obj->set_cantante($datos);
	}


	//--------------------------------------------------------------------

}
