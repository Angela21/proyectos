
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('props/bootstrap/css/bootstrap.css'); ?> ">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Hola CodeIgniter</title>
</head>
<body class="container">


  <form action="<?= base_url('home/ingresar') ?>" method="POST">
    <div>
      <label>Nombre</label>
      <input type="text" name="nombre">
    </div>
    <div>
      <label>Apellido</label>
      <input type="text" name="apellido">
    </div>
    <div>
      <label>Nombre Artistico</label>
      <input type="text" name="nombre_artistico">
    </div>
    <div>
     <label>Sexo</label>
     <select name="sexo">
       <option value="">Seleccione un sexo</option>
       <?php foreach ($sexo as $val) { ?>
        <option value="<?php echo $val->id_sexo ?>"><?php echo $val->sexo ?></option>
      <?php }  ?>
    </select>
  </div>
  <div>
    <input type="submit" name="INGRESAR" value="INGRESAR">
  </div>
</form>


<table border="1">
  <thead>
    <tr>
      <td>N°</td>
      <td>Nombre</td>
      <td>Apellido</td>
      <td>Nombre artistico</td>
      <td>Sexo</td>
      <td>Eliminar</td>
      <td>Editar</td>
    </tr>
  </thead>
  <tbody>
    <?php $n=1; foreach ($mostrar as $va) { ?>
      <tr>
        <td><?php echo $n; ?></td>
        <td><?php echo $va->nombre ?></td>
        <td><?php echo $va->apellido ?></td>
        <td><?php echo $va->nombre_artistico ?></td>
        <td><?php echo $va->sexo ?></td>
        <td><a href="<?php echo base_url('Home/eliminar/').$va->id_cantante; ?>" onclick="return confirm('Esta seguro de eliminar este registro?')"><button class="btn btn-danger">ELIMINAR</button></a></td>
        <td><a href="<?php echo base_url('Home/get_datos').$va->id_cantante; ?>"><button class="btn btn-primary">EDITAR</button></a></td>
      </tr>
      <?php $n++; }  ?>
    </tbody>

  </table>


</body>
</html>